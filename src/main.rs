use std::collections::HashMap;

#[derive(Debug)]
enum Cmp {
    Equals,
    NotEquals,
    Bigger,
    BiggerOrEqual,
    Smaller,
    SmallerOrEqual,
}

type Var = u8;

#[derive(Debug, Clone, Copy)]
enum Val {
    Var(Var),
    Lit(i32),
}

#[derive(Debug)]
struct Cond(Val, Cmp, Val);

#[derive(Debug)]
enum Op {
    Print(Val),
    Mov(Var, Val),
    Add(Var, Val),
    Sub(Var, Val),
    Mul(Var, Val),
    Jump(i32),
    If(Cond, i32),
    End,
}

fn cmp(s: &str) -> Cmp {
    match s {
        "==" => Cmp::Equals,
        "!=" => Cmp::NotEquals,
        "<" => Cmp::Bigger,
        "<=" => Cmp::BiggerOrEqual,
        ">" => Cmp::Smaller,
        ">=" => Cmp::SmallerOrEqual,
        &_ => panic!("Wrong"),
    }
}

fn var(s: char) -> Var {
    s as u8 - 'A' as u8
}

fn val(s: &str) -> Val {
    let chars: Vec<char> = s.chars().collect();

    match chars[0].is_alphabetic() {
        true => Val::Var(var(chars[0])),
        false => Val::Lit(s.parse().unwrap()),
    }
}

fn compile(src: Vec<&str>) -> Vec<Op> {
    let mut ops: Vec<Op> = Vec::new();

    let mut symbols: HashMap<String, usize> = HashMap::new();

    let mut int_symbols: Vec<String> = Vec::new();

    for line in src {
        let data: Vec<&str> = line.split(" ").collect();

        match data[0] {
            "PRINT" => ops.push(Op::Print(val(data[1]))),
            "MOV" => {
                let chars: Vec<char> = data[1].chars().collect();
                ops.push(Op::Mov(var(chars[0]), val(data[2])))
            }
            "ADD" => {
                let chars: Vec<char> = data[1].chars().collect();
                ops.push(Op::Add(var(chars[0]), val(data[2])))
            }
            "SUB" => {
                let chars: Vec<char> = data[1].chars().collect();
                ops.push(Op::Sub(var(chars[0]), val(data[2])))
            }
            "MUL" => {
                let chars: Vec<char> = data[1].chars().collect();
                ops.push(Op::Mul(var(chars[0]), val(data[2])))
            }
            "JUMP" => {
                let idx = int_symbols.len();
                int_symbols.push(data[1].to_string());
                ops.push(Op::Jump(idx.try_into().unwrap()))
            }
            "IF" => {
                let idx = int_symbols.len();
                int_symbols.push(data[5].to_string());
                ops.push(Op::If(
                    Cond(val(data[1]), cmp(data[2]), val(data[3])),
                    idx.try_into().unwrap(),
                ))
            }
            "END" => ops.push(Op::End),
            _ => {
                if data[0].chars().last().unwrap() == ':' {
                    symbols.insert(data[0].trim_end_matches(":").to_string(), ops.len() - 1);
                }
            }
        }
    }

    for op in &mut ops {
        match op {
            Op::Jump(i) => {
                let symbol = &int_symbols[*i as usize];
                *i = symbols[symbol].try_into().unwrap();
            }
            Op::If(_, i) => {
                let symbol = &int_symbols[*i as usize];
                *i = symbols[symbol].try_into().unwrap();
            }
            _ => {}
        }
    }

    ops
}

fn get_value(variables: &mut [i32], val: Val) -> i32 {
    match val {
        Val::Var(i) => variables[i as usize],
        Val::Lit(i) => i,
    }
}

fn eval(ops: Vec<Op>) -> Vec<i32> {
    let mut output: Vec<i32> = Vec::new();

    let mut variables = [0i32; 27];

    let mut i = 0;

    while i < ops.len() {
        match &ops[i] {
            Op::Print(a) => output.push(get_value(&mut variables, *a)),

            Op::Mov(x, a) => variables[*x as usize] = get_value(&mut variables, *a),
            Op::Add(x, a) => variables[*x as usize] += get_value(&mut variables, *a),
            Op::Sub(x, a) => variables[*x as usize] -= get_value(&mut variables, *a),
            Op::Mul(x, a) => variables[*x as usize] *= get_value(&mut variables, *a),
            Op::If(cond, idx) => {
                let x = get_value(&mut variables, cond.0);
                let y = get_value(&mut variables, cond.2);
                match cond.1 {
                    Cmp::Equals => {
                        if x == y {
                            i = *idx as usize
                        }
                    }
                    Cmp::NotEquals => {
                        if x != y {
                            i = *idx as usize
                        }
                    }
                    Cmp::Bigger => {
                        if x < y {
                            i = *idx as usize
                        }
                    }
                    Cmp::BiggerOrEqual => {
                        if x <= y {
                            i = *idx as usize
                        }
                    }
                    Cmp::Smaller => {
                        if x > y {
                            i = *idx as usize
                        }
                    }
                    Cmp::SmallerOrEqual => {
                        if x >= y {
                            i = *idx as usize
                        }
                    }
                }
            }
            Op::Jump(idx) => i = *idx as usize,
            Op::End => break,
        }
        i += 1;
    }

    output
}

fn main() {
    // Example program that counts a factorial(10)
    
    let src = vec![
        String::from("MOV A 1"),
        String::from("MOV B 1"),
        String::from("begin:"),
        String::from("PRINT A"),
        String::from("ADD B 1"),
        String::from("MUL A B"),
        String::from("IF B <= 10 JUMP begin"),
        String::from("END"),
    ];
    let ops = compile(src.iter().map(|s| &s[..]).collect());
    let output = eval(ops);
    println!("{:?}", output);
}
